* Ch. 16 The Arts
* Study Guide

** 1. The three criteria used by TOK to help define art.
*** Intention of artist
*** Quality of work
*** Response of spectators
** 2. Define aesthetics.
*** The branch of philosophy which studies beauty and the arts
** 3. Criticisms of the intention criterion.
*** Doubt that simply intending something to be art makes it art
*** The quality of art can matter, because no one wants crappy art
*** Something that wasn't intended to be art can be retroactively considered as such
*** The intentions of the creator are not necessary nor sufficient to make something art.
** 4. Define skill
*** The level of technical competence of the artist; A painter should be able to paint a good likeness, a poet should be able to create well-crafted line
** 5. Define form
*** The way a work of art is put together, involves unity, order, rhythm, balance, harmony, etc.
** 6. Define content
*** What a work of art depicts (a face, a landscape, etc.)
** 7. Criticisms of the quality criterion
*** Originality counts
**** Kitsch art can be seen as worthless even though it's high quality
**** Some people are okay with forgeries that are of high quality because they look so good
*** Calendars and greeting cards may be of high quality, but it's unlikely for them to be considered art
** 8. Define kitsch
*** Derivative, clichéd art
** 9. Define canon
*** The collection of works of art considered by scholars to be the most important and influential
** 10. Be prepared to address the question “Is everything art?”
*** Okay
** 11. Be able to explain inexhaustibility
*** Sure
** 12. the paradox of aesthetic judgement
*** Conflict between ideals
**** The idea of "rules" or "standards" of art
**** "Beauty is in the eye of the beholder"
***** Subjectivity of art
** 13. concept of being disinterested
*** According to Kant, what distinguishes aesthetic judgments from personal tastes is that they are disinterested.
*** If you are going to judge a work of art on its merits, you shouldn't bring your biography with you.
*** We should try to go beyond our individual tastes and preferences so that we can appreciate it from a more universal standpoint.
** 14. universal standards in art
*** People usually have similar standards for art
*** However, these vary greatly and can be contradictory from person to person or even work to work
** 15. explaining claims from Komar and Melamid
*** Komar and Melamid were two Russian artists who found that a wide variety of people in different cultures preferred the same type of painting, in which the viewer could see without being seen.
*** This preference could be explained by biological causes, as our basic instincts would cause us to prefer places in which we are safe from predators and are able to view our surroundings.
*** Some of their other research showed that there are also similarities across cultures in people's music tastes.
*** This could be because the metronome of the human pulse is a biological basis for our sense of rhythm in music.
*** As with universal standards in art, this could be caused by the prevalence of American culture around the world.
** 16. cultural differences that affect/do not affect art
*** There are universal elements running throughout all cultures, but we should also be aware of the differences between them.
*** Often, there may be a similar basis in art in different cultures, but the style differs.
*** For example, in comparing Chinese vs. Western paintings, they may show the same thing but the styles are very different.
*** Same with Chinese vs. Western opera and baseball vs. cricket and different styles of dance.
** 17. define cultural perspectives
*** In relation to art, this means the views that cultures have about aesthetics (specifically what they do and don't like in art). This relates to the universality of art, as some cultures have different ideas about what is aesthetically pleasing and what is not. Be able to answer the extent to which these cultural perspectives affect the universality of art.
** 18. three theories about the nature of art
*** 1. Art as imitation - says the purpose of art is to copy/mirror reality
    2. Art as communication - artists are often trying to convey a message to a spectator
       3. Art as education - arts have a moral and educative role
** 19. define the mimetic theory
** 20. define avant-garde
** 21. (thinking about art as communication theory having two dimensions) two dimensions
** concerning horizontal and vertical
** 22. Plato versus Aristotle (be prepared to discuss their beliefs about art)
** 23. define catharsis
** 24. art as it relates to science and science as it relates to art
** 25. the need for reason, imagination, and beauty through art
** 26. discovered or invented?
** 27. how knowledge is gained through art
** 28. how science and art complement each other
** 29. define perception
